#include <iostream>
#include <string>
#include <cassert>

std::string encryptedEmailGet()
{
     return "CWX@FMXVYUVQCx^EPZBBFP[_";
}

std::string keyGet()
{
    return "1232399534324592029809817236432342";
}

std::string parse(std::string email, const std::string &key)
{
    assert(email.size() == key.size());
    for(std::string::size_type i = 0; i < email.size(); ++i)
    {
        email[i] ^= key[i];
    }
    return email;
}

int main(int, char*)
{
    std::cout << parse(encryptedEmailGet(), keyGet()) << std::endl; 
    return 0;
}

